package com.demoapp.alcodesonboard.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.demoapp.alcodesonboard.R;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyFriendAdapter extends RecyclerView.Adapter<MyFriendAdapter.ViewHolder> {

    private List<DataHolder> mData = new ArrayList<>();
    private Callbacks mCallbacks;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_friend, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindTo(mData.get(position), mCallbacks);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void setData(List<DataHolder> data) {
        if (data == null) {


            mData = new ArrayList<>();
        } else {
            mData = data;
        }
    }

    public void setCallbacks(Callbacks callbacks) {
        mCallbacks = callbacks;
    }

    public static class DataHolder {


        public Long id;

        public String first_name;

        public String avatar;


    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linearlayout_root)
        public LinearLayout root;

        @BindView(R.id.textview_friends_name)
        public TextView first_name;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
        }

        public void bindTo(DataHolder data, Callbacks callbacks) {
            resetViews();


            if (data != null) {
                first_name.setText(data.first_name);


                if (callbacks != null) {

                    if (first_name.getText().toString().equals("No Data")) {
                        root.setOnClickListener(null);

                    } else {
                        root.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {
                                callbacks.onListItemClicked(data);
                            }

                        });

                    }
                }
            }
        }

        public void resetViews() {
            first_name.setText("");
            root.setOnClickListener(null);

        }
    }

    public interface Callbacks {

        void onListItemClicked(DataHolder data);


    }
}
