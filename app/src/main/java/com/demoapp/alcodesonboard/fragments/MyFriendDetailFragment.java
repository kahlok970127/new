package com.demoapp.alcodesonboard.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.activities.MyNoteDetailActivity;
import com.demoapp.alcodesonboard.database.entities.MyFriend;
import com.demoapp.alcodesonboard.database.entities.MyNote;

import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class MyFriendDetailFragment extends Fragment {

    public static final String TAG = MyFriendDetailFragment.class.getSimpleName();

    private static final String ARG_LONG_MY_FRIEND_ID = "ARG_LONG_MY_FRIEND_ID";


    @BindView(R.id.edittext_email)
    protected TextInputEditText mEditTextTitle;

    @BindView(R.id.edittext_content)
    protected TextInputEditText mEditTextContent;

    private Unbinder mUnbinder;
    private Long mMyFriendId = 0L;

    public MyFriendDetailFragment() {
    }

    public static MyFriendDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(ARG_LONG_MY_FRIEND_ID, id);

        MyFriendDetailFragment fragment = new MyFriendDetailFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Check arguments pass from previous page.
        Bundle args = getArguments();

//        if (args != null) {
//            mMyFriendId = args.getLong(ARG_LONG_MY_NOTE_ID, 0);
//        }

        initView();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }


    private void initView() {
        //done   // TODO BAD practice, should move Database operations to Repository.


        if (mMyFriendId > 0) {
            viewmodels.MyFriendViewModel.MyFriendViewModel viewModel = new viewmodels.MyFriendViewModel.MyFriendViewModel(getActivity().getApplication());
            MyFriend myFriend = viewModel.view(mMyFriendId);

            if (myFriend != null) {

            } else {
                // Record not found.
                Toast.makeText(getActivity(), "Note not found.", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }
        }
    }
}
