package com.demoapp.alcodesonboard.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;

import com.demoapp.alcodesonboard.activities.MainActivity;
import com.demoapp.alcodesonboard.activities.MyFriendDetailActivity;

import com.demoapp.alcodesonboard.adapters.MyFriendAdapter;
import com.demoapp.alcodesonboard.gsonmodels.AllFriendModel;
import com.demoapp.alcodesonboard.gsonmodels.LoginModel;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.demoapp.alcodesonboard.utils.SharedPreferenceHelper;
import com.google.gson.GsonBuilder;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MyFriendFragment extends Fragment implements MyFriendAdapter.Callbacks {

    public static final String TAG = MyFriendFragment.class.getSimpleName();

    @BindView(R.id.recyclerview)
    protected RecyclerView mRecyclerView;

    private final int REQUEST_CODE_MY_FRIEND_DETAIL = 300;

    private Unbinder mUnbinder;
    private MyFriendAdapter mAdapter;
    private viewmodels.MyFriendViewModel.MyFriendViewModel mViewModel;


    public MyFriendFragment() {
    }

    public static MyFriendFragment newInstance() {
        return new MyFriendFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_friend, container, false);

        mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initView();
        initViewModel();
    }

    @Override
    public void onDestroy() {
        if (mUnbinder != null) {
            mUnbinder.unbind();
        }

        super.onDestroy();
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_CODE_MY_FRIEND_DETAIL && resultCode == MyFriendDetailActivity.RESULT_CONTENT_MODIFIED) {
            // Child report content is changed, re-load list.
            mViewModel.loadMyFriendAdapterList();
        }
    }

    @Override
    public void onListItemClicked(MyFriendAdapter.DataHolder data) {
        Intent intent = new Intent(getActivity(), MyFriendDetailActivity.class);
        intent.putExtra(MyFriendDetailActivity.EXTRA_LONG_MY_FRIEND_ID, data.id);

        startActivityForResult(intent, REQUEST_CODE_MY_FRIEND_DETAIL);
    }


    private void initView() {
        mAdapter = new MyFriendAdapter();
        mAdapter.setCallbacks(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false));
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void initViewModel() {
//        mViewModel = ViewModelProviders.of(this).get(MyNotesViewModel.class);
        mViewModel = new ViewModelProvider(this, new viewmodels.MyFriendViewModel.MyFriendViewModelFactory(getActivity().getApplication())).get(viewmodels.MyFriendViewModel.MyFriendViewModel.class);
        mViewModel.getMyFriendAdapterListLiveData().observe(getViewLifecycleOwner(), new Observer<List<MyFriendAdapter.DataHolder>>() {


            @Override
            public void onChanged(List<MyFriendAdapter.DataHolder> dataHolders) {
                mAdapter.setData(dataHolders);
                mAdapter.notifyDataSetChanged();


                // TODO check dataHolders has data or not.
                // TODO show list if have, otherwise show label: "No data"
                if (mAdapter.getItemCount() == 0) {

                    Toast.makeText(getActivity(), "No Data.", Toast.LENGTH_SHORT).show();

                    MyFriendAdapter.DataHolder NoData = new MyFriendAdapter.DataHolder();

                    NoData.first_name = "No Data";

                    dataHolders.add(NoData);


                }


            }

        });

        // Load data into adapter.
        mViewModel.loadMyFriendAdapterList();


    }
/*
    String url = BuildConfig.BASE_API_URL + "users?page=2";
    StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

        @Override
        public void onResponse(String response) {

//here


            // TODO bad practice, should check user is still staying in this app or not after server response.

            // Convert JSON string to Java object.
            AllFriendModel responseModel = new GsonBuilder().create().fromJson(response, AllFriendModel.class);

            List<MyFriendAdapter.DataHolder> dataHolders = new ArrayList<>();

            MyFriendAdapter.DataHolder dataHolder = new MyFriendAdapter.DataHolder();
            for (int i = 0; i < responseModel.data.length; i++) {

                dataHolder.id = responseModel.data[i].id;
                dataHolder.email = responseModel.data[i].email;
                dataHolder.first_name = responseModel.data[i].first_name;
                dataHolder.last_name = responseModel.data[i].last_name;
                dataHolder.avatar = responseModel.data[i].avatar;
                dataHolders.add(dataHolder);


            }


        }
    }, new Response.ErrorListener() {


        @Override
        public void onErrorResponse(VolleyError error) {
            Timber.e("d;;Display error: %s", error.getMessage());

            // TODO bad practice, should check user is still staying in this app or not after server response.


        }
    }) {


    };
*/

}
