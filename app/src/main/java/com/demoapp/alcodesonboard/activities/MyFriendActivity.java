package com.demoapp.alcodesonboard.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.R;
import com.demoapp.alcodesonboard.adapters.MyFriendAdapter;
import com.demoapp.alcodesonboard.adapters.MyNotesAdapter;
import com.demoapp.alcodesonboard.fragments.MyFriendFragment;
import com.demoapp.alcodesonboard.gsonmodels.AllFriendModel;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.google.gson.GsonBuilder;


import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import timber.log.Timber;

public class MyFriendActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_my_friend);

        ButterKnife.bind(this);

        FragmentManager fragmentManager = getSupportFragmentManager();

        if (fragmentManager.findFragmentByTag(MyFriendFragment.TAG) == null) {
            // Init fragment.
            fragmentManager.beginTransaction()
                    .replace(R.id.framelayout_fragment_holder, MyFriendFragment.newInstance(), MyFriendFragment.TAG)
                    .commit();
        }


/*
        String url = BuildConfig.BASE_API_URL + "users?page=2";
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

//here


                // TODO bad practice, should check user is still staying in this app or not after server response.

                // Convert JSON string to Java object.
                AllFriendModel responseModel = new GsonBuilder().create().fromJson(response, AllFriendModel.class);

                List<MyFriendAdapter.DataHolder> dataHolders = new ArrayList<>();

                MyFriendAdapter.DataHolder dataHolder = new MyFriendAdapter.DataHolder();
                for (int i = 0; i < responseModel.data.length; i++) {

                    dataHolder.id = responseModel.data[i].id;
                    dataHolder.email = responseModel.data[i].email;
                    dataHolder.first_name = responseModel.data[i].first_name;
                    dataHolder.last_name = responseModel.data[i].last_name;
                    dataHolder.avatar = responseModel.data[i].avatar;
                    dataHolders.add(dataHolder);


                }


            }
        }, new Response.ErrorListener() {


            @Override
            public void onErrorResponse(VolleyError error) {
                Timber.e("d;;Display error: %s", error.getMessage());

                // TODO bad practice, should check user is still staying in this app or not after server response.


            }
        }) {


        };

*/
    }
}
