package com.demoapp.alcodesonboard.database.entities;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;

import org.greenrobot.greendao.annotation.NotNull;

@Entity
public class MyFriend {

    @Id(autoincrement = true)
    private Long id;


    @NotNull
    private String email;

    @NotNull
    private String first_name;

    @NotNull
    private String last_name;

    @NotNull
    private String avatar;

    @Generated(hash = 1740948344)
    public MyFriend(Long id, @NotNull String email, @NotNull String first_name,
                    @NotNull String last_name, @NotNull String avatar) {
        this.id = id;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.avatar = avatar;
    }

    @Generated(hash = 15986203)
    public MyFriend() {
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return this.first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return this.last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAvatar() {
        return this.avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }


}
