package com.demoapp.alcodesonboard.gsonmodels;

public class AllFriendModel {

    public int page;
    public int per_page;
    public int total;
    public int total_pages;
    public FriendModel[] data;

    public static class FriendModel {

        public Long id;
        public String email;
        public String first_name;
        public String last_name;
        public String avatar;

    }

}
