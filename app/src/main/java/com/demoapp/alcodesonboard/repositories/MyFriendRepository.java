package com.demoapp.alcodesonboard.repositories;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.demoapp.alcodesonboard.BuildConfig;
import com.demoapp.alcodesonboard.adapters.MyFriendAdapter;
import com.demoapp.alcodesonboard.adapters.MyNotesAdapter;
import com.demoapp.alcodesonboard.database.entities.MyFriend;

import com.demoapp.alcodesonboard.gsonmodels.AllFriendModel;
import com.demoapp.alcodesonboard.utils.DatabaseHelper;
import com.demoapp.alcodesonboard.utils.NetworkHelper;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class MyFriendRepository {
    private static MyFriendRepository mInstance;

    private MutableLiveData<List<MyFriendAdapter.DataHolder>> mMyFriendAdapterListLiveData = new MutableLiveData<>();

    public static MyFriendRepository getInstance() {
        if (mInstance == null) {
            synchronized (MyFriendRepository.class) {
                mInstance = new MyFriendRepository();
            }
        }

        return mInstance;
    }

    private MyFriendRepository() {
    }

    public LiveData<List<MyFriendAdapter.DataHolder>> getMyFriendAdapterListLiveData() {
        return mMyFriendAdapterListLiveData;
    }

    public void loadMyFriendAdapterList(Context context) {
        List<MyFriendAdapter.DataHolder> dataHolders = new ArrayList<>();
        List<MyFriend> records = DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .loadAll();

        if (records != null) {
            for (MyFriend myFriend : records) {
                MyFriendAdapter.DataHolder dataHolder = new MyFriendAdapter.DataHolder();
                dataHolder.id = myFriend.getId();
                dataHolder.first_name = myFriend.getFirst_name();

                dataHolder.avatar = myFriend.getAvatar();
                dataHolders.add(dataHolder);
            }
        } else {
            String url = BuildConfig.BASE_API_URL + "users?page=2";
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

//here


                    // TODO bad practice, should check user is still staying in this app or not after server response.

                    // Convert JSON string to Java object.
                    AllFriendModel responseModel = new GsonBuilder().create().fromJson(response, AllFriendModel.class);

                    List<MyFriendAdapter.DataHolder> dataHolders = new ArrayList<>();

                    MyFriendAdapter.DataHolder dataHolder = new MyFriendAdapter.DataHolder();
                    for (int i = 0; i < responseModel.data.length; i++) {

                        dataHolder.id = responseModel.data[i].id;
                        dataHolder.email = responseModel.data[i].email;
                        dataHolder.first_name = responseModel.data[i].first_name;
                        dataHolder.last_name = responseModel.data[i].last_name;
                        dataHolder.avatar = responseModel.data[i].avatar;
                        dataHolders.add(dataHolder);


                    }
                    loadMyFriendAdapterList(context);

                }
            }, new Response.ErrorListener() {


                @Override
                public void onErrorResponse(VolleyError error) {
                    Timber.e("d;;Display error: %s", error.getMessage());

                    // TODO bad practice, should check user is still staying in this app or not after server response.


                }
            }) {


            };
            NetworkHelper.getRequestQueueInstance(context).add(stringRequest);
        }
        mMyFriendAdapterListLiveData.setValue(dataHolders);

    }


    public MyFriend view(Context context, Long id) {
        MyFriend myfriend = DatabaseHelper.getInstance(context)
                .getMyFriendDao()
                .load(id);
        return myfriend;


    }
}
