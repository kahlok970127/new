package viewmodels.MyFriendViewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.demoapp.alcodesonboard.adapters.MyFriendAdapter;

import com.demoapp.alcodesonboard.database.entities.MyFriend;

import com.demoapp.alcodesonboard.repositories.MyFriendRepository;


import java.util.List;

public class MyFriendViewModel extends AndroidViewModel {

    private MyFriendRepository mMyFriendRepository;

    public MyFriendViewModel(@NonNull Application application) {
        super(application);

        mMyFriendRepository = MyFriendRepository.getInstance();
    }

    public LiveData<List<MyFriendAdapter.DataHolder>> getMyFriendAdapterListLiveData() {
        return mMyFriendRepository.getMyFriendAdapterListLiveData();
    }

    public void loadMyFriendAdapterList() {
        mMyFriendRepository.loadMyFriendAdapterList(getApplication());
    }


    public MyFriend view(Long id) {

        return mMyFriendRepository.view(getApplication(), id);

    }


}
